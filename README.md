# gitlab-ci-typst

A simplistic example of a GitLab CI pipeline to compile typst documents.

To use it, copy the `.gitlab-ci.yml` file to your own GitLab repository.

This CI configuration will:
- Compile a `main.typ` file in the root of the repository into `main.pdf`
- Store `main.pdf` as a [build artifact](https://docs.gitlab.com/ee/ci/jobs/job_artifacts.html) (so the PDF survives to the next step in the CI pipeline)
- Publish everything in [gitlab pages](https://docs.gitlab.com/ee/user/project/pages/). In this case, the final document will be at https://ivansanchez.gitlab.io/gitlab-ci-typst/main.pdf

It's possible to edit this pipeline, to compile different or multiple documents, or to integrate it with more complex build pipelines. Be sure to read about how GitLab CI works; see https://docs.gitlab.com/ee/ci/ .

(The CI pipeline depends on a docker image by 123marvin123. See https://hub.docker.com/r/123marvin123/typst )


# Legalese

This work is public domain (or "as public domain as possible"). Feel free to copy-paste this into your CI pipelines at your heart's content. See `LICENSE` for details.
